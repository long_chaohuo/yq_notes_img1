#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
@version: Anaconda
@author: LeYongkang
@contact: 1363989042@qq.com
@software: PyCharm
@file: Sentinel_Download
@time: 2021/9/04 0020 下午 9:23
参考文档：http://www.acgeospatial.co.uk/sentinelsat_demo/
"""
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date
import time
import datetime
import os

def download_sentinel_data(user_name,password,website,foot_print,start_date,end_date,platformname,max_cloud):

    print("进入函数")

    download_dir = r"G:\哨兵数据库\珠江口\2022\sel(珠江口2022年03齐全)\sentinel-2"

    # 判断下载文件夹在不在，如果不在，自动建立子目录
    if not os.path.exists(download_dir):
        os.makedirs(download_dir)

    api = SentinelAPI(user_name,password,website)
    products = api.query(
                     foot_print,
                     date=(start_date, end_date),
                     platformname=platformname, # 可选： Sentinel-1,Sentinel-2,Sentinel-3,Sentinel-5 Precursor
                     producttype = 'S2MSI2A',   # Sentinel-2 可选: S2MSI2A,S2MSI1C, S2MS2Ap
                     cloudcoverpercentage = (0, max_cloud)
    )

    print(len(products))

    while True:
        for product in products:
            product_info = api.get_product_odata(product)
            # print(product_info)
            # 获取文件 id ，通过 id 号下载文件
            product_id = product_info['id']
            # 获取文件 title，title + .zip 为完整的文件名
            product_title = product_info['title']
            print("{}".format(product_id))
            print("{}".format(product_title))

            product_info = api.get_product_odata(product_id)
            is_online = product_info['Online']

            # 判断是否在线
            if is_online:   # 如果在线，执行
                print('编号为 {} 的产品在线。'.format(product_id))
                if not os.path.isfile(download_dir + os.sep + product_title + ".zip"):   # 单账号的时候使用
                    print('本地无 {}.zip 的完整文件。'.format(product_title))
                    # if not os.path.isfile( download_dir + os.sep + product_title + ".zip.incomplete" ):  # 双账号的时候
                    # print('本地无 {}.zip.incomplete 的未完成文件。开始下载'.format(product_title))
                    # 注意：上次下载一半，下次重启代码，循环到上次下载的文件时，会继续下载（接着.zip.incomplete继续下载）
                    api.download(product_id,directory_path= download_dir)

            else:   # 如果不在线，需要触发一次，然后跳过，（出发后，会在半小时后转化为在线）等待后续再下载
                print('编号为 {} 的产品不在线。'.format(product_id))

                if not os.path.isfile(download_dir + os.sep + product_title + ".zip"):  # 单账号的时候使用
                    print('本地无 {}.zip 的完整文件，尝试触发 LongTermArchive 库'.format(product_title))

                    try: # 尝试触发
                        api.download(product_info['id'], directory_path= download_dir)
                        api.trigger_offline_retrieval(product_id)
                        break  # if successful, move past
                    except Exception as e:
                        print("[ERROR] Request failed.. sleeping for 31 mins before retrying (current time: {})".format(
                            datetime.datetime.now()))
                        #     time.sleep(60 * 31)

        # 离线的 每 30 min 才能提交一次请求
        time.sleep(60 * 15)



if __name__ == '__main__':

    # 有时候单个账号访问次数多了，会限制访问，大约办个小时后解禁；限制访问的时候，可以使用另一个账号
    # user_name="username"
    # password = "password"

    user_name="longchaohuo3"
    password = "5720389a"

    website = 'https://scihub.copernicus.eu/dhus'
    # map.geojson 限制了下载数据的范围
    # 来 https://geojson.io/#map=2/20.0/0.0 网站，自己画个区域就行；然后 copy 到 map.geojson里面就可以
    foot_print = geojson_to_wkt(read_geojson(r'G:\哨兵数据库\珠江口\2022\sel(珠江口2022年03齐全)\map.geojson'))

    start_date = '20220101'
    end_date = date(2022,12, 20)  # 这样的 2021-09-13 格式
    print(end_date)
    # 下载的数据类型
    platformname = 'Sentinel-2'
    # 控制云量最大值
    max_cloud = 95
    # print("test")
    download_sentinel_data(user_name, password, website, foot_print, start_date, end_date, platformname,max_cloud)

