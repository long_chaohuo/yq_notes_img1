# coding:utf-8
import arcpy
import sys
from arcpy import env
import time

reload(sys)
sys.setdefaultencoding('utf8')


def get_time_stamp():
    # 获取毫秒级时间戳
    # 参考：https://blog.csdn.net/qq_27061049/article/details/105396418/
    ct = time.time()
    local_time = time.localtime(ct)
    data_head = time.strftime("%Y%m%d%H%M%S", local_time)
    data_secs = (ct - int(ct)) * 1000
    time_stamp = "%s%03d" % (data_head, data_secs)
    return time_stamp


# 创建全色锐化栅格数据集—数据管理工具箱 | ArcGIS Desktop
# https://pro.arcgis.com/zh-cn/pro-app/tool-reference/data-management/create-pansharpened-raster-dataset.htm

# 创建全色锐化栅格数据集—帮助 | ArcGIS Desktop
# https://desktop.arcgis.com/zh-cn/arcmap/10.4/tools/data-management-toolbox/create-pansharpened-raster-dataset.htm

# 压缩（环境设置）—帮助 | ArcGIS Desktop
# https://desktop.arcgis.com/zh-cn/arcmap/10.4/tools/environments/compression.htm


def printme(str):
    "打印任何传入的字符串"
    print(str)
    return


def pansharpenronghe(imageurl):
    print("开始时间：" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
    out_put_folder_path = "output\\"
    out_put_file_path = out_put_folder_path + get_time_stamp() + ".tif"

    # 输出文件不压缩
    arcpy.env.compression = "NONE"

    arcpy.CreatePansharpenedRasterDataset_management(
        "GF2_XXX-MSS1.tiff", "3", "2", "1", "4", out_put_file_path,
        "GF2_XXX--PAN1.tiff", "Gram-Schmidt", "0.166", "0.167", "0.167",
        "0.5", "")
    print('结束时间：', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))


if __name__ == '__main__':
    pansharpenronghe("")